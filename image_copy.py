#!/usr/bin/env python3

import argparse
import config
import os

from vote import vote


def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)


def get_args():
    parser = argparse.ArgumentParser()

    pgroup = parser.add_argument_group('required named arguments')

    pgroup.add_argument('--in',
                        help='Directory of images to choose from',
                        type=dir_path, dest='in_',
                        required=True)
    pgroup.add_argument('--out',
                        help='Directory to save images to',
                        type=dir_path, dest='out_',
                        required=True)

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_args()
    config.ROOT_DIR = args.out_

    config.CATEGORIES = os.listdir(config.ROOT_DIR)
    if not config.CATEGORIES:
        config.CATEGORIES = ['']

    vote(args.in_, 'Image Selection')
