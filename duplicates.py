from PIL import Image
import argparse
import imagehash
import os


def get_hash(path):
    img = Image.open(path)
    h = imagehash.phash(img)
    return h


def get_hashes_file(path, existing_images):
    f = open(path, 'r')
    # hash image_name

    hashs = {}
    images = []
    lines = f.readlines()
    for line in lines:
        line = line.strip()

        h = line.split(' ')[0]
        image = ' '.join(line.split(' ')[1:])

        if image not in existing_images:
            continue
        
        if h not in hashs.keys():
            hashs[h] = []
        hashs[h].append(image)
        images.append(image)

    return hashs, images


def get_similar_images(path, hashfile='hashs.txt'):
    images = os.listdir(path)
    hashs, images_done = get_hashes_file(hashfile, images)

    f = open(hashfile, 'a')

    for i, image in enumerate(images):
        if image in images_done:
            print(f'[{i}] Skipping already processed image')
            continue

        h = get_hash(os.path.join(path, image))

        print(f'[{i}] Got hash for image: {h}')
        f.write(f'{h} {image}\n')

        if h not in hashs.keys():
            hashs[h] = []
        hashs[h].append(image)

    for key in list(hashs.keys()):
        if len(hashs[key]) == 1:
            del hashs[key]

    f.close()

    return hashs


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('directory',
                        help='Directory containing the images to compare')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = get_args()
    
    hashs = get_similar_images(args.directory)
    for h, images in hashs.items():
        print('Similar images founds:')
        prefix = 'https://pics.hatrix.fr/portraits/'
        [print(f'  {prefix}{img}') for img in images]

