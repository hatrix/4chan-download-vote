#!/usr/bin/env python3

import argparse
from pygame.locals import *
from PIL import Image
import config
import shutil
import pygame
import sys
import os
import re
from pathlib import Path


def textHollow(font, message, fontcolor):
    notcolor = [c^0xFF for c in fontcolor]
    base = font.render(message, 0, fontcolor, notcolor)
    size = base.get_width() + 2, base.get_height() + 2
    img = pygame.Surface(size, 16)
    img.fill(notcolor)
    base.set_colorkey(0)
    img.blit(base, (0, 0))
    img.blit(base, (2, 0))
    img.blit(base, (0, 2))
    img.blit(base, (2, 2))
    base.set_colorkey(0)
    base.set_palette_at(1, notcolor)
    img.blit(base, (1, 1))
    img.set_colorkey(notcolor)
    return img


def textOutline(font, message, fontcolor, outlinecolor):
    base = font.render(message, 0, fontcolor)
    outline = textHollow(font, message, outlinecolor)
    img = pygame.Surface(outline.get_size(), 16)
    img.blit(base, (1, 1))
    img.blit(outline, (0, 0))
    img.set_colorkey(0)
    return img

   
def display(screen, path, size):
    screen.fill(tuple([190] * 3))
    width = size[0]
    height = size[1]

    try:
        s = pygame.image.load(path)
        w, h = s.get_width(), s.get_height()

        if w > size[0] or h > size[1]:
            ratio = width / w if  w > h else height / h
            w = int(ratio * w)
            h = int(ratio * h)
    
        s = pygame.transform.scale(s, (w, h))
        screen.blit(s, (width/2 - w/2, height/2 - h/2))
    except:
        pass


def write_current(screen, font, current, length, name, cur_cat):
    outline = (250, 250, 250)
    fontcolor = (1, 1, 1)

    number = textOutline(font, "{}/{}".format(current+1, length), fontcolor, outline)
    cat = textOutline(font, "{}/".format(config.CATEGORIES[cur_cat]), fontcolor, outline)
    name = textOutline(font, "{}".format(name), fontcolor, outline)

    screen.blit(number, (10, 10))
    screen.blit(cat, (10, 40))
    screen.blit(name, (10, 70))
    pygame.display.flip()


def vote(directory, title):
    pygame.init()
    pygame.display.set_caption(title)
    size = 1000, 1000
    screen = pygame.display.set_mode(size)
    screen.fill((238, 238, 238))
    font = pygame.font.SysFont("loma", 30)


    exts = ('jpg', 'png', 'gif', 'tiff', 'bmp', 'jpeg', 'tif')
    imgs = []
    images = sorted(Path(directory).iterdir(), key=os.path.getmtime)
    images = [i.name for i in images]
    for img in images:
        if img.lower().endswith(exts):
            sizes = Image.open(os.path.join(directory, img)).size
            if sizes[0] >= config.MIN_SIZE or sizes[1] >= config.MIN_SIZE:
                imgs.append(img)

    try:
        imgs.remove('link')
    except:
        pass

    current = 0
    cur_cat = 0
    display(screen, os.path.join(directory, imgs[current]), size)
    write_current(screen, font, current, len(imgs), imgs[current], cur_cat)

    go = True
    while go:
        pygame.time.Clock().tick(60)
        event = pygame.event.poll()
        if event.type == QUIT:
            exit()

        if event.type == KEYDOWN:
            if event.key == K_ESCAPE or event.key == K_q:
                exit()
            if event.key == K_RETURN:
                src = os.path.join(directory, imgs[current])
                dst = os.path.join(config.ROOT_DIR, config.CATEGORIES[cur_cat])
                dst = os.path.join(dst, imgs[current])

                shutil.copyfile(src, dst)
                print("Copying from {} to {}".format(src, dst))
            if event.key == K_DELETE:
                dst = os.path.join(config.ROOT_DIR, config.CATEGORIES[cur_cat])

                if imgs[current] in os.listdir(dst):
                    os.remove(os.path.join(dst, imgs[current]))
                    print("Image removed !")
                else:
                    print("Image not saved, nothing to remove")

            if event.key == K_LEFT:
                current = (current - 1) % len(imgs)
            if event.key == K_RIGHT:
                current = (current + 1) % len(imgs)
            if event.key == K_c:
                cur_cat = (cur_cat + 1) % len(config.CATEGORIES)
            if event.key == K_UP:
                current = (current + 10) % len(imgs)
            if event.key == K_DOWN:
                current = (current - 10) % len(imgs)

            display(screen, os.path.join(directory, imgs[current]), size)
            write_current(screen, font, current, len(imgs), imgs[current], 
                          cur_cat)
