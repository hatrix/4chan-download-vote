#!/usr/bin/env python3
import argparse
import workerpool
import requests
import urllib.request
import config
import shutil
import sys
import os
import re
from pathlib import Path

from vote import vote


def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)


def write_link(path, link):
    f = open(os.path.join(path, 'link'), 'w')
    f.write(link + '\n')
    f.close()


class downloaded:
    nb_max = 0
    nb_down = 0


def download_img(img):
    url = img[0]
    where = os.path.join(img[1], img[0].split('/')[-1])

    url = url.strip()
    try:
        urllib.request.urlretrieve(url, where)
    except:
        print("Image {}/{} couldn't be downloaded".format(downloaded.nb_down+1, downloaded.nb_max))
        downloaded.nb_down += 1
        return
    
    downloaded.nb_down += 1
    print("Downloaded {}/{}".format(downloaded.nb_down, downloaded.nb_max))


def download(url=None):
    if url is None:
        url = input('4chan link: ')
        print('')
    r = requests.get(url)

    title = ''.join(r.text.split('<title>')[1].split('</title>')[0].split('-')[1:-2]).strip()
    imgs = ['http:' + img.split('href="')[1].split('" target')[0] for img in r.text.split('File:')][1:]
    imgs = [img for img in imgs if not img.endswith(('.webm', '.gif'))]
   
    original_title = title
    title = re.sub('[?()#$^&%@\/|\[\]{}<>.,\\\]', '_', title)
    
    board = url.split('/')[3]
    thread_id = url.split('/')[5]

    title = '{}-{}-{}'.format(board, thread_id, title)
    full_title = os.path.join(config.SAVE_TO, title)

    if title not in os.listdir(config.SAVE_TO):
        os.mkdir(full_title)
        print('Creating directory "{}"'.format(title))
    else:
        print('Completing directory "{}"'.format(title))
    
    if 'link' not in os.listdir(full_title):
        write_link(full_title, url)
    
    to_down = []
    count = 0
    for n, img in enumerate(imgs):
        if img.split('/')[-1] not in os.listdir(full_title):
            to_down.append((img, full_title, n, len(imgs)))
        else:
            count += 1

    downloaded.nb_max = len(imgs)
    downloaded.nb_down = count
    pool = workerpool.WorkerPool(size=5)
    pool.map(download_img, to_down)
    pool.shutdown()
    pool.wait()

    if len(imgs) == count:
        count = -1
    
    return original_title, full_title, count


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('link',
                        help="4chan's thread link",
                        type=str)
    parser.add_argument('--save_to',
                        help='Root directory to save the whole thread to',
                        type=dir_path,
                        dest='save_to')
    parser.add_argument('--out',
                        help='Root directory to select and save the images to',
                        type=dir_path,
                        dest='out_')
    parser.add_argument('--categories',
                        help='Sub directories where to save the images',
                        nargs='*')

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    count = 0

    args = get_args()
    print(args, end='\n\n')

    # If there are no categories in the config, take what's in the out dir
    if len(config.CATEGORIES) == 0:
        config.CATEGORIES = os.listdir(config.ROOT_DIR)

    # Check if we got categories through the parser
    if args.categories:
        config.CATEGORIES = args.categories

    # Check if we got a directory as output
    if args.out_:
        config.ROOT_DIR = args.out_

    # Check if we're saving the thread to a specific location
    if args.save_to:
        config.SAVE_TO = args.save_to

    print(config.CATEGORIES)
    print(config.ROOT_DIR)
            
    title, directory, count = download(url=args.link)

    if count > 0:
        print('\nDownload started at {}'.format(count+1))
    elif count < 0:
        print('\nNo image downloaded, thread complete')
    else: # == 0
        print('\nAll pictures were downloaded')

    input('Ready to copy ? Press enter!')
    print('')

    vote(directory, title)
